package com.riiid.project.model

import java.util.*
import javax.persistence.*

@Entity
class Attendee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    var name: String? = null
    var company: String? = null
    var email: String? = null
    var registered: Date? = null
    var bio : String? = null

    override fun toString(): String =
        "attendee(id: ${id}, name: ${name}, company: ${company}, " +
                "email: ${email}, registered: ${registered}, bio: ${bio})"
}

package com.riiid.project.model

import java.util.*
import javax.persistence.*

@Entity
class Talk {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private val id: Long? = null
    private val room: Int? = null

    @ManyToOne
    private val speaker: Attendee? = null

    @ManyToMany
    private var attendees: MutableList<Attendee>? = ArrayList<Attendee>()
}

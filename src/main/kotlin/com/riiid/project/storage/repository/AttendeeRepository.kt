package com.riiid.project.storage.repository

import com.riiid.project.model.Attendee

import org.springframework.data.repository.CrudRepository

interface AttendeeRepository: CrudRepository<Attendee, Long>

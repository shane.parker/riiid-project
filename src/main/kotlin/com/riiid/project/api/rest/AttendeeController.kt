package com.riiid.project.api.rest

import com.riiid.project.model.Attendee
import com.riiid.project.service.AttendeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI


@RestController
class AttendeeController {
    @Autowired
    private lateinit var attendeeService: AttendeeService

    @GetMapping("/api/attendee/{id}")
    fun getAttendee(@PathVariable(value = "id") id: Long): ResponseEntity<Attendee> {
        val attendee = attendeeService.load(id) ?: return ResponseEntity.notFound().build()
        return ResponseEntity.ok(attendee)
    }

    @PostMapping("/api/attendee/{id}")
    fun postAttendee(): ResponseEntity<Attendee>? {
        val attendee = Attendee()
        attendee.name = "derp"

        attendeeService.save(attendee)

        val uri: URI = ServletUriComponentsBuilder.fromCurrentRequest()
            .build().toUri()

        return ResponseEntity.created(uri).body(attendee)
    }
}

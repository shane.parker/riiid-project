package com.riiid.project.service

import com.riiid.project.model.Attendee
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

@Service
class AttendeeService {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    fun load(id: Long): Attendee? = entityManager.find(Attendee::class.java, id)

    @Transactional
    fun save(attendee: Attendee) = entityManager.persist(attendee)
}
